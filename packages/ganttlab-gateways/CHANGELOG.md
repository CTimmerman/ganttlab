# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 0.1.0 (2020-04-30)


### Features

* **ganttlab-gateways:** initial release ([a6a4c78](https://gitlab.com/ganttlab/ganttlab/commit/a6a4c7856df9a851ef54c0bd531c5dd9208ccbcc))

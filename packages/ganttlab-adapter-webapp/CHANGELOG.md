# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 0.1.0 (2020-04-30)


### Features

* **ganttlab-adapter-webapp:** initial release ([dcc1ad6](https://gitlab.com/ganttlab/ganttlab/commit/dcc1ad64a0bcff8278cf2fe87dda024ed8569ef0))

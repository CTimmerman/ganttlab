# GanttLab Privacy Policy

This privacy policy ("Privacy Policy") applies to all visitors and users of the GanttLab.org hosted services and websites (collectively, the "Website" or "Websites") and self-managed installations, which are offered by GanttLab and/or any of its affiliates ("GanttLab" or "we" or "us"). Please read this Privacy Policy carefully. By accessing or using any part of the Websites or self-managed installations, you acknowledge you have been informed of and consent to our practices with regard to your personal information and data.

## Data Protection

Should you wish to make modifications, deletions, or additions to any personal data you believe to be captured by GanttLab, or if you have any general security concerns, please reach out to contact@ganttlab.org.

## What Information GanttLab Collects and Why

### Information from Website Visitors

Like most website operators, GanttLab collects basic non-personally-identifying information from Website visitors of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. We collect this information to better understand how visitors use the Website, to improve our Websites and experience for visitors, and to monitor the security of the Websites. From time to time, GanttLab may publicly release non-personally-identifying information collected from Website visitors in the aggregate, e.g., by publishing a report on trends in the usage of the Website.

GanttLab also collects potentially personally-identifying information like Internet Protocol (IP) addresses from visitors. GanttLab does not use such information to identify or track individual visitors, however. We collect this information to understand how visitors use the Websites, to improve performance and content, and to monitor the security of the Websites.

GanttLab may collect statistics about the behavior of visitors to our Websites. For instance, GanttLab may reveal how many times a particular view was displayed using aggregated statistics that contain anonymous user information only.

### Information GanttLab Does Not Collect

GanttLab does not intentionally collect sensitive personal information, such as social security numbers, genetic data, health information, or religious information.

## How GanttLab Uses and Protects Personally-Identifying Information

### GanttLab Communications with You

If you send us a request (for example via email), we reserve the right to publish your request in order to help us clarify or respond to your request or to help us support other users. We will not publish your personally-identifiable information in connection with your request.

## Cookies, Tracking Technologies and Do Not Track

### Cookies

A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. GanttLab uses cookies to help GanttLab identify and track visitors, their usage of the Websites, and their Website access preferences. GanttLab visitors who do not wish to have cookies placed on their computers may set their browsers to refuse cookies before using the Websites. Disabling browser cookies may cause certain features of GanttLab's websites to not function properly.

Certain pages on the Website may set other third party cookies. For example, we may embed content, such as images, from another site that sets a cookie. These sites set their own cookies and we do not have access or control over these cookies. The use of cookies by third parties is not covered by our Privacy Policy.

### Tracking Technologies

We use third party tracking services, but we don’t use these services to track you individually or collect your personally identifiable-information. We use these services to collect information about how the Website performs and how users navigate through and use the Website so we can monitor and improve our content and Website performance.

Third party tracking services gather certain simple, non-personally identifying information over time, such as your IP address, browser type, internet service provider, referring and exit pages, timestamp, and similar data about your use of the Website. We do not link this information to any of your personal information such as your user name.

### Do Not Track

"Do Not Track" is a privacy preference [you can set in your browser](https://allaboutdnt.com/). GanttLab does respect this setting by totally disabling Tracking Technologies accordingly.

## Data Retention and Deletion

Please note that due to the open source nature of our products, services, and community, we may retain limited personally-identifiable information indefinitely in order to ensure transactional integrity and nonrepudiation. For example, if you contribute to a GanttLab project and provide your personal information in connection with that contribution, that information (including your name) will be embedded and publicly displayed with your contribution and we will not be able to delete or erase it because doing so would break the project code.

## Contacting GanttLab About Your Privacy

If you have questions or concerns about the way we are handling your information, or would like to exercise your privacy rights, please email us with the subject line "Privacy Concern" at contact@ganttlab.org.

## Privacy Policy Changes

Although most changes are likely to be minor, GanttLab may change its privacy policy from time to time, and in GanttLab's sole discretion. If we decide to make a significant change to our Privacy Statement, we will post a notice on the homepage of our website for a period of time after the change is made.

GanttLab encourages visitors to frequently check this page for any minor changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.

[Privacy policy change history](https://gitlab.com/ganttlab/ganttlab/commits/master/docs/privacy-policy.md)